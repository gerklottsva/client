#ifndef CALLBACK_H
#define CALLBACK_H

#include <QObject>
#include <tango.h>

class Callback : public QObject, public Tango::CallBack
{
    Q_OBJECT
public:
    explicit Callback(QObject *parent = nullptr);
    virtual void push_event(Tango::EventData *ed);

signals:
    void boolUpdated(bool value, QString name);
    void dblUpdated(double value, QString name);
    void shortUpdated(short value, QString name);
};

#endif // CALLBACK_H
