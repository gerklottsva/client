#ifndef TABLEMANAGER_H
#define TABLEMANAGER_H

#include <QObject>
#include <QFile>
#include <QDebug>

class TableManager : public QObject
{
    Q_OBJECT
public:
    explicit TableManager(QObject *parent = nullptr);

    template<typename dtype>
    QVector<dtype> arange(dtype inStart, dtype inStop, dtype inStep = 1)
    {
        if (inStep > 0 && inStop < inStart)
        {
            qDebug("stop value must be larger than the start value for positive step.");
        }

        if (inStep < 0 && inStop > inStart)
        {
            qDebug("start value must be larger than the stop value for negative step.");
        }

        QVector<dtype> values;

        dtype theValue = inStart;
        auto counter = dtype{ 1 };

        if (inStep > 0)
        {
            while (theValue < inStop)
            {
                values.push_back(theValue);
                theValue = inStart + inStep * counter++;
            }
        }
        else
        {
            while (theValue > inStop)
            {
                values.push_back(theValue);
                theValue = inStart + inStep * counter++;
            }
        }

        return values;
    }

    template<typename dtype>
    QVector<QVector<dtype>> interpolateLinear(QVector<QVector<dtype>> points, dtype step)
    {
        QVector<QVector<dtype>> result;
        result.push_back(QVector<dtype>());
        result.push_back(QVector<dtype>());
        for(int i = 0; i < points[0].size()-1;i++)
        {
            QVector<dtype> values = arange<dtype>(points[0][i],points[0][i+1],step);
            for(auto x : values)
            {
                result[0].push_back(x);
                result[1].push_back(points[1][i] + (points[1][i+1]-points[1][i])/(points[0][i+1]-points[0][i])*(x-points[0][i]));
            }
        }
        return result;
    }

    void writeTableToFile(QString filename, QVector<QVector<double>> points);
    QVector<QVector<double>> generateTable(QVector<double> &time, QVector<double> &voltage, double step);

    bool table1Generated = false;
    bool table2Generated = false;

signals:

};

#endif // TABLEMANAGER_H
