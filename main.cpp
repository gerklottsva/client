#include "mainwindow.h"
#include <QApplication>
#include <QtConcurrent>
#include <QThread>
#include <QFuture>

void init(MainWindow* w)
{
    try {
        Tango::DeviceProxy *device = new Tango::DeviceProxy("booster/rf/station");

        Callback *cb = new Callback(w);
        QObject::connect(cb, &Callback::dblUpdated, w, &MainWindow::updateDblLabel);
        QObject::connect(cb, &Callback::boolUpdated, w, &MainWindow::updateBoolLabel);
        QObject::connect(cb, &Callback::shortUpdated, w, &MainWindow::updateShortLabel);

        std::vector<string> attributes = {"UplS1U1Progress","UplS1U2Progress","UplS2U1Progress","UplS2U2Progress","field_sensor","start_magnetic_field","K_a","K_b","K_c","Mod1Ready", "Mod2Ready", "Mod1Status", "Mod2Status","K_S1_Uzazor","K_S1_Uamp","K_S1_Ianode1","K_S1_Ianode2","K_S1_Uanode","K_S1_Uekran","K_S1_Unakal1", "K_S1_Unakal2","K_S1_Usetka","K_S2_Uzazor","K_S2_Uamp","K_S2_Ianode1","K_S2_Ianode2","K_S2_Uanode","K_S2_Uekran","K_S2_Unakal1", "K_S2_Unakal2","K_S2_Usetka"};

        for(int i = 0; i < attributes.size(); i++)
        {
            device->poll_attribute(attributes[i], 500);
        }

        for(int i = 0; i < attributes.size(); i++)
        {
            device->subscribe_event(attributes[i], Tango::EventType::CHANGE_EVENT, cb);
        }

        QObject::connect(w, &MainWindow::appClosed, w, [&]
        {
            delete device;
            delete cb;
        });


    }  catch (Tango::DevFailed &e) {
        qDebug() << e.errors[0].desc << endl;
    }
}

int main(int argc, char *argv[])
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowTitle("NICA MAIN");
    w.show();

    //QFuture<void> future = QtConcurrent::run(&init, &w);

    init(&w);

    return a.exec();
}
