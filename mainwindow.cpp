#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::getCloseStateWindow()
{
    return windowClosed;
}

void MainWindow::addPointToTheEnd(double u, double t)
{
    if(ui->rdoBtnStage1->isChecked())
    {
        tableManager.table1Generated = false;
        qv_u1.append(std::ceil(u*100)/100);
        qv_t1.append(std::ceil(t*100)/100);
    }
    else if(ui->rdoBtnStage2->isChecked())
    {
        tableManager.table2Generated = false;
        qv_u2.append(std::ceil(u*100)/100);
        qv_t2.append(std::ceil(t*100)/100);
    }
    QString str_u1,str_u2,str_t1,str_t2;
    for(int i = 0; i < qv_u1.size(); i++)
    {
        str_u1.append(QString::number(qv_u1[i]));
        str_u1 += ";   ";
    }
    for(int i = 0; i < qv_t1.size(); i++)
    {
        str_t1.append(QString::number(qv_t1[i]));
        str_t1 += ";   ";
    }
    for(int i = 0; i < qv_u2.size(); i++)
    {
        str_u2.append(QString::number(qv_u2[i]));
        str_u2 += ";   ";
    }
    for(int i = 0; i < qv_t2.size(); i++)
    {
        str_t2.append(QString::number(qv_t2[i]));
        str_t2 += ";   ";
    }
    ui->lblHV1_U->setText("U: " + str_u1);
    ui->lblHV1_T->setText("t:  " + str_t1);
    ui->lblHV2_U->setText("U: " + str_u2);
    ui->lblHV2_T->setText("t:  " + str_t2);
}

void MainWindow::removePointFromTheEnd()
{
    if(ui->rdoBtnStage1->isChecked())
    {
        if(!qv_u1.empty() && !qv_t1.empty())
        {
            tableManager.table1Generated = false;
            qv_u1.pop_back();
            qv_t1.pop_back();
        }
    }
    else if(ui->rdoBtnStage2->isChecked())
    {
        if(!qv_u2.empty() && !qv_t2.empty())
        {
            tableManager.table2Generated = false;
            qv_u2.pop_back();
            qv_t2.pop_back();
        }
    }
    QString str_u1,str_u2,str_t1,str_t2;
    for(int i = 0; i < qv_u1.size(); i++)
    {
        str_u1.append(QString::number(qv_u1[i]));
        str_u1 += ";   ";
    }
    for(int i = 0; i < qv_t1.size(); i++)
    {
        str_t1.append(QString::number(qv_t1[i]));
        str_t1 += ";   ";
    }
    for(int i = 0; i < qv_u2.size(); i++)
    {
        str_u2.append(QString::number(qv_u2[i]));
        str_u2 += ";   ";
    }
    for(int i = 0; i < qv_t2.size(); i++)
    {
        str_t2.append(QString::number(qv_t2[i]));
        str_t2 += ";   ";
    }
    ui->lblHV1_U->setText("U: " + str_u1);
    ui->lblHV1_T->setText("t:  " + str_t1);
    ui->lblHV2_U->setText("U: " + str_u2);
    ui->lblHV2_T->setText("t:  " + str_t2);
}

void MainWindow::addPointAtIndex(double u, double t, int index)
{
    if(ui->rdoBtnStage1->isChecked())
    {
        if(index <= qv_u1.size()+1)
        {
            tableManager.table1Generated = false;
            qv_u1.insert(index-1,std::ceil(u*100)/100);
            qv_t1.insert(index-1,std::ceil(t*100)/100);
        }
    }
    else if(ui->rdoBtnStage2->isChecked())
    {
        if(index <= qv_u2.size()+1)
        {
            tableManager.table2Generated = false;
            qv_u2.insert(index-1,std::ceil(u));
            qv_t2.insert(index-1,std::ceil(t));
        }
    }
    QString str_u1,str_u2,str_t1,str_t2;
    for(int i = 0; i < qv_u1.size(); i++)
    {
        str_u1.append(QString::number(qv_u1[i]));
        str_u1 += ";   ";
    }
    for(int i = 0; i < qv_t1.size(); i++)
    {
        str_t1.append(QString::number(qv_t1[i]));
        str_t1 += ";   ";
    }
    for(int i = 0; i < qv_u2.size(); i++)
    {
        str_u2.append(QString::number(qv_u2[i]));
        str_u2 += ";   ";
    }
    for(int i = 0; i < qv_t2.size(); i++)
    {
        str_t2.append(QString::number(qv_t2[i]));
        str_t2 += ";   ";
    }
    ui->lblHV1_U->setText("U: " + str_u1);
    ui->lblHV1_T->setText("t:  " + str_t1);
    ui->lblHV2_U->setText("U: " + str_u2);
    ui->lblHV2_T->setText("t:  " + str_t2);
}

void MainWindow::removePointAtIndex(int index)
{
    if(ui->rdoBtnStage1->isChecked())
    {
        if(index <= qv_u1.size())
        {
            tableManager.table1Generated = false;
            qv_u1.remove(index-1);
            qv_t1.remove(index-1);
        }
    }
    else if(ui->rdoBtnStage2->isChecked())
    {
        if(index <= qv_u2.size())
        {
            tableManager.table2Generated = false;
            qv_u2.remove(index-1);
            qv_t2.remove(index-1);
        }
    }
    QString str_u1,str_u2,str_t1,str_t2;
    for(int i = 0; i < qv_u1.size(); i++)
    {
        str_u1.append(QString::number(qv_u1[i]));
        str_u1 += "; \t";
    }
    for(int i = 0; i < qv_t1.size(); i++)
    {
        str_t1.append(QString::number(qv_t1[i]));
        str_t1 += "; \t";
    }
    for(int i = 0; i < qv_u2.size(); i++)
    {
        str_u2.append(QString::number(qv_u2[i]));
        str_u2 += "; \t";
    }
    for(int i = 0; i < qv_t2.size(); i++)
    {
        str_t2.append(QString::number(qv_t2[i]));
        str_t2 += "; \t";
    }
    ui->lblHV1_U->setText("U: " + str_u1);
    ui->lblHV1_T->setText("t:  " + str_t1);
    ui->lblHV2_U->setText("U: " + str_u2);
    ui->lblHV2_T->setText("t:  " + str_t2);
}

void MainWindow::clearData()
{
    if(ui->rdoBtnStage1->isChecked() && ui->rdoBtnStage2->isChecked())
    {
        tableManager.table1Generated = false;
        tableManager.table2Generated = false;
        qv_u1.clear();
        qv_t1.clear();
        qv_u2.clear();
        qv_t2.clear();
    }
    else if(ui->rdoBtnStage1->isChecked())
    {
        tableManager.table1Generated = false;
        qv_u1.clear();
        qv_t1.clear();
    }
    else if(ui->rdoBtnStage2->isChecked())
    {
        tableManager.table2Generated = false;
        qv_u2.clear();
        qv_t2.clear();
    }
}

void MainWindow::replotUZazor()
{
    ui->plotUZazor->graph(0)->setData(qv_t1, qv_u1);
    ui->plotUZazor->graph(1)->setData(qv_t2, qv_u2);
    ui->plotUZazor->replot();
    ui->plotUZazor->update();
}

void MainWindow::plotUZazor()
{
    ui->plotUZazor->addGraph();
    ui->plotUZazor->addGraph();
    ui->plotUZazor->addGraph();
    ui->plotUZazor->addGraph();
    ui->plotUZazor->addGraph();
    ui->plotUZazor->addGraph();
    ui->plotUZazor->graph(0)->setName("Writed high voltage STAGE 1");
    ui->plotUZazor->graph(1)->setName("Writed high voltage STAGE 2");
    ui->plotUZazor->graph(2)->setName("Readed high voltage station 1");
    ui->plotUZazor->graph(3)->setName("Readed high voltage station 2");
    ui->plotUZazor->graph(4)->setName("Setted high voltage station 1");
    ui->plotUZazor->graph(5)->setName("Setted high voltage station 2");
    ui->plotUZazor->legend->setVisible(true);
    ui->plotUZazor->xAxis->setLabel("Time, seconds");
    ui->plotUZazor->yAxis->setLabel("U,zazor, kV");
    ui->plotUZazor->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->plotUZazor->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
    ui->plotUZazor->graph(0)->setPen(QPen(Qt::green));
    ui->plotUZazor->graph(1)->setScatterStyle(QCPScatterStyle::ssCircle);
    ui->plotUZazor->graph(1)->setPen(QPen(Qt::blue));
    ui->plotUZazor->graph(2)->setPen(QPen(Qt::red));
    ui->plotUZazor->graph(3)->setPen(QPen(Qt::yellow));
    ui->plotUZazor->graph(4)->setPen(QPen(Qt::black));
    ui->plotUZazor->graph(4)->setScatterStyle(QCPScatterStyle::ssCircle);
    ui->plotUZazor->graph(5)->setPen(QPen(Qt::black));
    replotUZazor();
}

void MainWindow::plotField()
{
    ui->plotField->addGraph();
    ui->plotField->graph(0)->setName("Readed magnetic field");
    ui->plotField->legend->setVisible(true);
    ui->plotField->xAxis->setLabel("Time, seconds");
    ui->plotField->yAxis->setLabel("Magnetic field, Tesla");
    ui->plotField->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->plotField->graph(0)->setPen(QPen(Qt::blue));
}

void MainWindow::plotIAnode()
{
    QVector<QString> graphNames ({"Readed I anode 1, station 1","Readed I anode 2, station 1","Readed I anode 1, station 2","Readed I anode 2, station 2"});
    for(int i = 0; i < 4; i++)
    {
        ui->plotIAnode->addGraph();
    }
    for(int i = 0; i < 4; i++)
    {
        ui->plotIAnode->graph(i)->setName(graphNames[i]);
    }
    ui->plotIAnode->legend->setVisible(true);
    ui->plotIAnode->xAxis->setLabel("Time, seconds");
    ui->plotIAnode->yAxis->setLabel("I anode, A");
    ui->plotIAnode->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->plotIAnode->graph(0)->setPen(QPen(Qt::red));
    ui->plotIAnode->graph(1)->setPen(QPen(Qt::yellow));
    ui->plotIAnode->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
    ui->plotIAnode->graph(1)->setScatterStyle(QCPScatterStyle::ssCircle);
    ui->plotIAnode->graph(2)->setPen(QPen(Qt::red));
    ui->plotIAnode->graph(3)->setPen(QPen(Qt::yellow));
    ui->plotIAnode->setBackground(QBrush(QColor("black")));
    ui->plotIAnode->xAxis->setLabelColor("white");
    ui->plotIAnode->xAxis->setTickLabelColor("white");
    ui->plotIAnode->xAxis->setBasePen(QPen(Qt::white));
    ui->plotIAnode->xAxis->setSubTickPen(QPen(Qt::white));
    ui->plotIAnode->yAxis->setLabelColor("white");
    ui->plotIAnode->yAxis->setTickLabelColor("white");
    ui->plotIAnode->yAxis->setBasePen(QPen(Qt::white));
    ui->plotIAnode->yAxis->setSubTickPen(QPen(Qt::white));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();
    QMessageBox msgBx;

    QAbstractButton *yes = msgBx.addButton("Yes", QMessageBox::YesRole);
    QAbstractButton *no = msgBx.addButton("No", QMessageBox::NoRole);
    msgBx.setText("Вы уверены, что хотите закрыть приложение?");
    msgBx.setWindowTitle("Closing");
    msgBx.exec();
    if(msgBx.clickedButton() == yes)
    {
        emit appClosed();
        windowClosed = true;
        event->accept();
    }
}

void MainWindow::clickedGraph(QMouseEvent *event)
{
    QPoint point = event->pos();
    if(ui->plotUZazor->yAxis->pixelToCoord(point.y()) < 5 && ui->plotUZazor->yAxis->pixelToCoord(point.y()) > -0.1)
    {
        addPointToTheEnd(ui->plotUZazor->yAxis->pixelToCoord(point.y()),ui->plotUZazor->xAxis->pixelToCoord(point.x()));
        replotUZazor();
    }
}

void MainWindow::updateShortLabel(short value, QString name)
{
    if(name.contains("upls1u1progress"))
    {
        ui->prgBrS1U1->setValue(value);
    }
    else if(name.contains("upls2u1progress"))
    {
        ui->prgBrS1U2->setValue(value);
    }
    else if(name.contains("upls1u2progress"))
    {
        ui->prgBrS2U1->setValue(value);
    }
    else if(name.contains("upls2u2progress"))
    {
        ui->prgBrS2U2->setValue(value);
    }
}

void MainWindow::updateDblLabel(double value, QString name)
{
    if(name.contains("k_a"))
    {
        ui->lndtACoeff->setText(QString::number(value));
    }
    else if(name.contains("k_b"))
    {
        ui->lndtBCoeff->setText(QString::number(value));
    }
    else if(name.contains("k_c"))
    {
        ui->lndtCCoeff->setText(QString::number(value));
    }
    else if(name.contains("start_magnetic_field"))
    {
        ui->lndtStartField->setText(QString::number(value));
    }
    else if(name.contains("field_sensor"))
    {
        ui->lndtFieldSensor->setText(QString::number(value));
    }
    else if(name.contains("k_s1_ianode1"))
    {
        ui->lblIanode1Station1->setText(QString::number(value));
        ui->lblIanode1Station1_2->setText(QString::number(value));
    }
    else if(name.contains("k_s1_ianode2"))
    {
        ui->lblIanode2Station1->setText(QString::number(value));
        ui->lblIanode2Station1_2->setText(QString::number(value));
    }
    else if(name.contains("k_s1_ianode"))
    {
        ui->lblIanodeStation1->setText(QString::number(value));
    }
    else if(name.contains("k_s1_uamp"))
    {
        ui->lblUampStation1->setText(QString::number(value));
        ui->lblUampStation1_2->setText(QString::number(value));
    }
    else if(name.contains("k_s1_uanode"))
    {
        ui->lblUanodeStation1->setText(QString::number(value));
        ui->lblUanodeStation1_2->setText(QString::number(value));
    }
    else if(name.contains("k_s1_uekran"))
    {
        ui->lblUekranStation1->setText(QString::number(value));
        ui->lblUekranStation1_2->setText(QString::number(value));
    }
    else if(name.contains("k_s1_unakal1"))
    {
        ui->lblUnakal1Station1->setText(QString::number(value));
        ui->lblUnakal1Station1_2->setText(QString::number(value));
    }
    else if(name.contains("k_s1_unakal2"))
    {
        ui->lblUnakal2Station1->setText(QString::number(value));
        ui->lblUnakal2Station1_2->setText(QString::number(value));
    }
    else if(name.contains("k_s1_usetka"))
    {
        ui->lblUsetkaStation1->setText(QString::number(value));
        ui->lblUsetkaStation1_2->setText(QString::number(value));
    }
    else if(name.contains("k_s1_uzazor"))
    {
        ui->lblUzazorStation1->setText(QString::number(value));
        ui->lblUzazorStation1_2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_ianode1"))
    {
        ui->lblIanode1Station2->setText(QString::number(value));
        ui->lblIanode1Station2_2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_ianode2"))
    {
        ui->lblIanode2Station2->setText(QString::number(value));
        ui->lblIanode2Station2_2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_ianode"))
    {
        ui->lblIanodeStation2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_uamp"))
    {
        ui->lblUampStation2->setText(QString::number(value));
        ui->lblUampStation2_2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_uanode"))
    {
        ui->lblUanodeStation2->setText(QString::number(value));
        ui->lblUanodeStation2_2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_uekran"))
    {
        ui->lblUekranStation2->setText(QString::number(value));
        ui->lblUekranStation2_2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_unakal1"))
    {
        ui->lblUnakal1Station2->setText(QString::number(value));
        ui->lblUnakal1Station2_2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_unakal2"))
    {
        ui->lblUnakal2Station2->setText(QString::number(value));
        ui->lblUnakal2Station2_2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_usetka"))
    {
        ui->lblUsetkaStation2->setText(QString::number(value));
        ui->lblUsetkaStation2_2->setText(QString::number(value));
    }
    else if(name.contains("k_s2_uzazor"))
    {
        ui->lblUzazorStation2->setText(QString::number(value));
        ui->lblUzazorStation2_2->setText(QString::number(value));
    }
}

void MainWindow::updateBoolLabel(bool value, QString name)
{
    if(name.contains("mod1ready"))
    {
        if(value == true)
        {
            ui->lblReadyModulatorIndication1->setText("Ready");
        }
        else
        {
            ui->lblReadyModulatorIndication1->setText("Not ready");
        }
    }
    if(name.contains("mod2ready"))
    {
        if(value == true)
        {
            ui->lblReadyModulatorIndication2->setText("Ready");
        }
        else
        {
            ui->lblReadyModulatorIndication2->setText("Not ready");
        }
    }
    if(name.contains("mod1status"))
    {
        if(value == true)
        {
            ui->lblStatusModulatorIndicator1->setText("ON");
        }
        else
        {
            ui->lblStatusModulatorIndicator1->setText("OFF");
        }
    }
    if(name.contains("mod2status"))
    {
        if(value == true)
        {
            ui->lblStatusModulatorIndicator2->setText("ON");
        }
        else
        {
            ui->lblStatusModulatorIndicator2->setText("OFF");
        }
    }
}

void MainWindow::init()
{
    plotUZazor();
    plotField();
    plotIAnode();
    cycleMaker.sendCommandToDevice("booster/rf/station","UpdCoefficients");

    connect(ui->plotUZazor, SIGNAL(mouseDoubleClick(QMouseEvent *)), SLOT(clickedGraph(QMouseEvent *)));

    connect(ui->btnUpdtFieldAndUZazor, &QPushButton::clicked,[&](){
        QVector<double> readedUZazor1 = cycleMaker.getAttributeData("S1Uzazor");
        QVector<double> readedUZazor2 = cycleMaker.getAttributeData("S2Uzazor");
        QVector<double> readedSetUZazor1 = cycleMaker.getAttributeData("S1UzazorSet");
        QVector<double> readedSetUZazor2 = cycleMaker.getAttributeData("S2UzazorSet");
        QVector<double> step, set_zazor_step;

        step.push_back(0.01);
        for(int i = 1; i < readedUZazor1.size(); i++)
        {
            step.push_back(step[i-1] + 0.01);
        }

        set_zazor_step.push_back(0.01);
        for(int i = 1; i < readedSetUZazor1.size(); i++)
        {
            set_zazor_step.push_back(set_zazor_step[i-1] + 0.01);
        }

        ui->plotUZazor->graph(2)->setData(step, readedUZazor1);
        ui->plotUZazor->graph(3)->setData(step, readedUZazor2);
        ui->plotUZazor->graph(4)->setData(set_zazor_step, readedSetUZazor1);
        ui->plotUZazor->graph(5)->setData(set_zazor_step, readedSetUZazor2);
        ui->plotUZazor->replot();
        ui->plotUZazor->update();
    });

    connect(ui->btnUpdtFieldAndUZazor, &QPushButton::clicked,[&](){
    QVector<double> readedField = cycleMaker.getAttributeData("Field");
    QVector<double> step;
    step.push_back(0.01);
    for(int i = 1; i < readedField.size(); i++)
    {
        step.push_back(step[i-1] + 0.01);
    }
    ui->plotField->graph(0)->setData(step, readedField);
    ui->plotField->replot();
    ui->plotField->update();
    });

    connect(ui->btnUpdtIAnode, &QPushButton::clicked,[&](){
        QVector<double> readedIAnode1S1 = cycleMaker.getAttributeData("S1Ianode1");
        QVector<double> readedIAnode2S1 = cycleMaker.getAttributeData("S1Ianode2");
        QVector<double> readedIAnode1S2 = cycleMaker.getAttributeData("S2Ianode1");
        QVector<double> readedIAnode2S2 = cycleMaker.getAttributeData("S2Ianode2");

        QVector<double> step;
        step.push_back(0.01);
        for(int i = 1; i < readedIAnode1S1.size(); i++)
        {
            step.push_back(step[i-1] + 0.01);
        }

        ui->plotIAnode->graph(0)->setData(step, readedIAnode1S1);
        ui->plotIAnode->graph(1)->setData(step, readedIAnode2S1);
        ui->plotIAnode->graph(2)->setData(step, readedIAnode1S2);
        ui->plotIAnode->graph(3)->setData(step, readedIAnode2S2);
        ui->plotIAnode->replot();
        ui->plotIAnode->update();
    });

    connect(ui->btnAddPointToTheEnd, &QPushButton::clicked,[&](){
        addPointToTheEnd(ui->bx_u->value(), ui->bx_t->value());
        replotUZazor();
    });
    connect(ui->btnRemovePointFromTheEnd, &QPushButton::clicked,[&](){
        removePointFromTheEnd();
        replotUZazor();
    });
    connect(ui->btnAddAtIndex, &QPushButton::clicked,[&](){
        addPointAtIndex(ui->bx_u->value(), ui->bx_t->value(), ui->bx_index->value());
        replotUZazor();
    });
    connect(ui->btnRemoveAtIndex, &QPushButton::clicked,[&](){
        removePointAtIndex(ui->bx_index->value());
        replotUZazor();
    });
    connect(ui->btnClearPoints, &QPushButton::clicked,[&](){
        clearData();
        replotUZazor();
        QString str_u1,str_u2,str_t1,str_t2;
        for(int i = 0; i < qv_u1.size(); i++)
        {
            str_u1.append(QString::number(qv_u1[i]));
            str_u1 += ";   ";
        }
        for(int i = 0; i < qv_t1.size(); i++)
        {
            str_t1.append(QString::number(qv_t1[i]));
            str_t1 += ";   ";
        }
        for(int i = 0; i < qv_u2.size(); i++)
        {
            str_u2.append(QString::number(qv_u2[i]));
            str_u2 += ";   ";
        }
        for(int i = 0; i < qv_t2.size(); i++)
        {
            str_t2.append(QString::number(qv_t2[i]));
            str_t2 += ";   ";
        }
        ui->lblHV1_U->setText("U: " + str_u1);
        ui->lblHV1_T->setText("t:  " + str_t1);
        ui->lblHV2_U->setText("U: " + str_u2);
        ui->lblHV2_T->setText("t:  " + str_t2);
    });

    connect(ui->btnManualControl, &QPushButton::clicked, ui->stackedWidget, [&](){
        ui->stackedWidget->setCurrentIndex(0);

        cycleMaker.sendCommandToDevice("booster/rf/station","UpdCoefficients");
    });
    connect(ui->btnFieldAndFrequency, &QPushButton::clicked, ui->stackedWidget, [&](){
        ui->stackedWidget->setCurrentIndex(1);
    });
    connect(ui->btnIAnode, &QPushButton::clicked, ui->stackedWidget, [&](){
        ui->stackedWidget->setCurrentIndex(2);
    });
    connect(ui->btnSettings, &QPushButton::clicked, ui->stackedWidget, [&](){
        ui->stackedWidget->setCurrentIndex(ui->stackedWidget->count() - 1);

        cycleMaker.sendCommandToDevice("booster/rf/station","UpdCoefficients");
    });

    connect(ui->btnDownloadNewDataFieldPage, &QPushButton::clicked, [&]()
    {
        cycleMaker.sendCommandToDevice("booster/rf/station","UpdS1");
        cycleMaker.sendCommandToDevice("booster/rf/station","UpdS2");
        cycleMaker.sendCommandToDevice("booster/rf/station", "UpdFieldFreq");
    });

    connect(ui->btnDownloadNewDataIAnodePage, &QPushButton::clicked, [&]()
    {
        cycleMaker.sendCommandToDevice("booster/rf/station","UpdS1");
        cycleMaker.sendCommandToDevice("booster/rf/station","UpdS2");
    });

    connect(ui->btnGenerateTablesStg1, &QPushButton::clicked, [&](){
        if(!qv_u1.isEmpty())
        {
            qv_stage1_interpolated = tableManager.generateTable(qv_t1,qv_u1, cycleMaker.deltaT_controller);
            qv_stage1_interpolated = tableManager.generateTable(qv_t1,qv_u1, cycleMaker.deltaT_controller);
            tableManager.writeTableToFile("controller_HV1Stage1", qv_stage1_interpolated);
            tableManager.writeTableToFile("controller_HV2Stage1", qv_stage1_interpolated);
            tableManager.table1Generated = true;
        }
        else
        {
            QMessageBox::information(this, "Exception", "There is no data to generate tables");
        }
    });
    connect(ui->btnGenerateTablesStg2, &QPushButton::clicked, [&](){
        if(!qv_u2.isEmpty())
        {
            qv_stage2_interpolated = tableManager.generateTable(qv_t2,qv_u2, cycleMaker.deltaT_controller);
            qv_stage2_interpolated = tableManager.generateTable(qv_t2,qv_u2, cycleMaker.deltaT_controller);
            tableManager.writeTableToFile("controller_HV1Stage2", qv_stage2_interpolated);
            tableManager.writeTableToFile("controller_HV2Stage2", qv_stage2_interpolated);
            tableManager.table2Generated = true;
        }
        else
        {
            QMessageBox::information(this, "Exception", "There is no data to generate tables");
        }
    });

    /*
    connect (&cycleThread, &QThread::started, &cycleMaker, &CycleMaker::makeCycle);
    connect (&cycleMaker, &CycleMaker::finished, &cycleThread, &QThread::terminate);
    cycleMaker.moveToThread(&cycleThread);
    */

    connect(ui->rdoBtnDerivativeMode, &QRadioButton::pressed, [&]()
    {
        cycleMaker.setMode("0");
        qDebug() << "Derivative mode";
    });

    connect(ui->rdoBtnDirectMode, &QRadioButton::pressed, [&]()
    {
        cycleMaker.setMode("1");
        qDebug() << "Direct mode";
    });

    connect(ui->rdoBtnTableMode, &QRadioButton::pressed, [&]()
    {
        cycleMaker.setMode("2");
        qDebug() << "Table mode";
    });

    if(ui->rdoBtnDerivativeMode->isChecked())
    {
        cycleMaker.setMode("0");
        qDebug() << "Derivative mode";
    }
    else if(ui->rdoBtnDirectMode->isChecked())
    {
        cycleMaker.setMode("1");
        qDebug() << "Direct mode";
    }
    else if(ui->rdoBtnTableMode->isChecked())
    {
        cycleMaker.setMode("2");
        qDebug() << "Table mode";
    }

    connect(ui->btnStartCycle, &QPushButton::clicked, [&](){

        if(ui->rdoBtnSingleMode->isChecked() && !ui->checkBoxUseTester->isChecked())
        {
            cycleMaker.setTesterMode(false);
            cycleMaker.setCycleModeState(false);
            cycleMaker.makeCycle();
            qDebug() << "SingleMode";
        }
        else if(ui->rdoBtnCycleMode->isChecked() && !ui->checkBoxUseTester->isChecked())
        {
            //countForStartThreadControl++;
            //ui->btnStartCycle->setCheckable(true);
            cycleMaker.setTesterMode(false);
            cycleMaker.setCycleModeState(true);
            cycleMaker.makeCycle();
            //if(countForStartThreadControl==1) cycleThread.start();
            qDebug() << "CycleMode";
        }
        else if(ui->rdoBtnSingleMode->isChecked() && ui->checkBoxUseTester->isChecked())
        {
            cycleMaker.setTesterMode(true);
            cycleMaker.setCycleModeState(false);
            cycleMaker.makeCycle();
            qDebug() << "TesterSingleMode";
        }
        else if(ui->rdoBtnCycleMode->isChecked() && ui->checkBoxUseTester->isChecked())
        {
            //countForStartThreadControl++;
            //ui->btnStartCycle->setCheckable(true);
            cycleMaker.setTesterMode(true);
            cycleMaker.setCycleModeState(true);
            cycleMaker.makeCycle();
            //if(countForStartThreadControl==1) cycleThread.start();
            qDebug() << "CycleTesterMode";
        }
    });
    connect(ui->btnStopCycle, &QPushButton::clicked, [&](){
        //countForStartThreadControl = 0;
        try {
            Tango::DeviceProxy device("booster/rf/station");
            cycleMaker.setCycleModeState(false);
            Tango::DeviceAttribute boolArgin;
            boolArgin << cycleMaker.cycleModeState();
            boolArgin.set_name("RepeatCycle");
            device.write_attribute(boolArgin);
            qDebug() << "StopCycle";
        }  catch (Tango::DevFailed &e) {
            QMessageBox::information(this, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
        }
    });

    connect(ui->btnONModulator1, &QPushButton::clicked, [&](){
        cycleMaker.sendCommandToDevice("booster/rf/station", "Mod1On");
    });
    connect(ui->btnONModulator2, &QPushButton::clicked, [&](){
        cycleMaker.sendCommandToDevice("booster/rf/station", "Mod2On");
    });
    connect(ui->btnOFFModulator1, &QPushButton::clicked, [&](){
        cycleMaker.sendCommandToDevice("booster/rf/station", "Mod1Off");
    });
    connect(ui->btnOFFModulator2, &QPushButton::clicked, [&](){
        cycleMaker.sendCommandToDevice("booster/rf/station", "Mod2Off");
    });
    connect(ui->btnUpldTblU1S1, &QPushButton::clicked, [&](){  // 1 станция 1 этап
        if(tableManager.table1Generated == true)
        {
            cycleMaker.sendCommandToDevice("booster/rf/station", "UploadS1U1", qv_stage1_interpolated[1]);
        }
        else
        {
            QMessageBox::information(this,"Exception","You have not generated the tables");
        }
    });
    connect(ui->btnUpldTblU1S2, &QPushButton::clicked, [&](){ // 1 станция 2 этап
        if(tableManager.table1Generated == true)
        {
            cycleMaker.sendCommandToDevice("booster/rf/station", "UploadS1U2", qv_stage2_interpolated[1]);
        }
        else
        {
            QMessageBox::information(this,"Exception","You have not generated the tables");
        }
    });
    connect(ui->btnUpldTblU2S1, &QPushButton::clicked, [&](){ // 2 станция 1 этап
        if(tableManager.table2Generated == true)
        {
            cycleMaker.sendCommandToDevice("booster/rf/station", "UploadS2U1", qv_stage1_interpolated[1]);
        }
        else
        {
            QMessageBox::information(this,"Exception","You have not generated the tables");
        }
    });
    connect(ui->btnUpldTblU2S2, &QPushButton::clicked, [&](){ // 2 станция 2 этап
        if(tableManager.table2Generated == true)
        {
            cycleMaker.sendCommandToDevice("booster/rf/station", "UploadS2U2", qv_stage2_interpolated[1]);
        }
        else
        {
            QMessageBox::information(this,"Exception","You have not generated the tables");
        }
    });

    connect(ui->btnSetCoefficients, &QPushButton::clicked,[&](){
        cycleMaker.writeAttributeData("K_a", ui->lndtACoeff->text().toDouble());
        cycleMaker.writeAttributeData("K_b", ui->lndtBCoeff->text().toDouble());
        cycleMaker.writeAttributeData("K_c", ui->lndtCCoeff->text().toDouble());
        cycleMaker.writeAttributeData("start_magnetic_field", ui->lndtStartField->text().toDouble());
        cycleMaker.writeAttributeData("field_sensor", ui->lndtFieldSensor->text().toDouble());
    });
    connect(ui->btnDefault, &QPushButton::clicked, [&]()
    {
        cycleMaker.sendCommandToDevice("booster/rf/station", "Load");
    });
    connect(ui->btnSave, &QPushButton::clicked, [&]()
    {
        cycleMaker.sendCommandToDevice("booster/rf/station", "Save");
    });

    connect(ui->btnSetUZazorS1, &QPushButton::clicked, [&]()
    { 
        if(ui->lndtSetUZazorS1->isModified())
        {
            cycleMaker.sendCommandToDevice("booster/rf/station", "SetS1UZazor", ui->lndtSetUZazorS1->text().toFloat());
        }
        else
        {
            QMessageBox::information(this, "Exception", "There is not entered value");
        }
    });
    connect(ui->btnSetUZazorS2, &QPushButton::clicked, [&]()
    { 
        if(ui->lndtSetUZazorS2->isModified())
        {
            cycleMaker.sendCommandToDevice("booster/rf/station", "SetS2UZazor", ui->lndtSetUZazorS2->text().toFloat());
        }
        else
        {
            QMessageBox::information(this, "Exception", "There is not entered value");
        }
    });
    connect(ui->btnSetIAnodeS1, &QPushButton::clicked, [&]()
    {
        if(ui->lndtSetIAnodeS1->isModified())
        {
            cycleMaker.sendCommandToDevice("booster/rf/station", "SetS1IAnode", ui->lndtSetIAnodeS1->text().toFloat());
        }
        else
        {
            QMessageBox::information(this, "Exception", "There is not entered value");
        }
    });
    connect(ui->btnSetIAnodeS2, &QPushButton::clicked, [&]()
    {
        if(ui->lndtSetIAnodeS2->isModified())
        {
            cycleMaker.sendCommandToDevice("booster/rf/station", "SetS2IAnode", ui->lndtSetIAnodeS2->text().toFloat());
        }
        else
        {
            QMessageBox::information(this, "Exception", "There is not entered value");
        }
    });
    connect(ui->btnSetManualFreq, &QPushButton::clicked, [&]()
    {
        if(ui->lndtSetDDSFrequency->isModified())
        {
            cycleMaker.sendCommandToDevice("booster/rf/station", "SetDDSFrequency", ui->lndtSetDDSFrequency->text().toFloat());
        }
        else
        {
            QMessageBox::information(this, "Exception", "There is not entered value");
        }
    }); 

    connect(ui->btnRebootCPU, &QPushButton::clicked, [&]()
    {
        cycleMaker.sendCommandToDevice("booster/rf/station", "RebootCPU");
    });
}
