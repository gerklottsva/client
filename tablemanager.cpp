#include "tablemanager.h"

TableManager::TableManager(QObject *parent)
    : QObject{parent}
{

}

void TableManager::writeTableToFile(QString filename, QVector<QVector<double>> points)
{
    QFile file(filename);
    if(file.open(QIODevice::WriteOnly))
    {
        QByteArray data;
        for(int i = 0; i < points[1].size(); i++)
        {
            data.append(QString::number(points[1][i]));
            data.append("\n");
        }
        file.write(data);
        file.close();
        qDebug() << "Wrote to the file";
    }
    else
    {
        qDebug() << file.errorString();
    }
}

QVector<QVector<double>> TableManager::generateTable(QVector<double> &time, QVector<double> &voltage, double step)
{
    QVector<QVector<double>> points;
    points.push_back(QVector<double>());
    points.push_back(QVector<double>());
    points[0] = time;
    points[1] = voltage;

    QVector<QVector<double>> interpolated_points = interpolateLinear(points,step);

    return interpolated_points;
}
