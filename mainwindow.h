#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <tango.h>
#include <QDebug>
#include <QPushButton>
#include <QMessageBox>
#include <QCloseEvent>
#include <QVector>
#include "callback.h"

#include <QThread>
#include "cyclemaker.h"

#include "tablemanager.h"

#include <QFile>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool getCloseStateWindow();

    void addPointToTheEnd(double u, double t);
    void removePointFromTheEnd();
    void addPointAtIndex(double u, double t, int index);
    void removePointAtIndex(int index);
    void clearData();

    void replotUZazor();
    void plotUZazor();
    void plotField();
    void plotIAnode();



protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void clickedGraph(QMouseEvent *event);

public slots:
    void updateDblLabel(double value, QString name);
    void updateBoolLabel(bool value, QString name);
    void updateShortLabel(short value, QString name);

signals:
    void appClosed();

private:
    Ui::MainWindow *ui;
    bool windowClosed = false;
    void init();

    CycleMaker cycleMaker;
    //QThread cycleThread;
    //int countForStartThreadControl = 0;

    TableManager tableManager;

    QVector<double> qv_u1, qv_t1, qv_u2, qv_t2;
    QVector<QVector<double>> qv_stage1_interpolated, qv_stage2_interpolated;


};
#endif // MAINWINDOW_H
