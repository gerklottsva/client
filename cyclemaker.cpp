#include "cyclemaker.h"
#include "mainwindow.h"

CycleMaker::CycleMaker(QObject *parent)
    : QObject{parent}
{

}

CycleMaker::~CycleMaker()
{

}

void CycleMaker::makeCycle()
{
    try
    {
        Tango::DeviceProxy device("booster/rf/station");

        Tango::DeviceAttribute boolArgin;
        boolArgin << m_cycleModeState;
        boolArgin.set_name("RepeatCycle");
        device.write_attribute(boolArgin);

        device.command_inout("Cycle");
        qDebug() << "Cycle";

        /*
        do{

            Tango::DeviceAttribute repeatCycle = device.read_attribute("RepeatCycle");
            repeatCycle >> m_cycleModeState;
            if(m_cycleModeState)
            {
                QThread::msleep(m_cycleDurationInMilliseconds);
            }
            else
            {
                break;
            }
        }while(m_cycleModeState);
        */

    }catch (Tango::DevFailed &e) {
        qDebug() << e.errors[0].desc << endl;
    }
    emit finished();
}

bool CycleMaker::cycleModeState() const
{
    return m_cycleModeState;
}

void CycleMaker::setCycleModeState(bool newCycleModeState)
{
    if (m_cycleModeState == newCycleModeState)
        return;
    m_cycleModeState = newCycleModeState;
    emit cycleModeStateChanged();
}

int CycleMaker::cycleDurationInMilliseconds() const
{
    return m_cycleDurationInMilliseconds;
}

void CycleMaker::setCycleDurationInMilliseconds(int newCycleDurationInMilliseconds)
{
    if (m_cycleDurationInMilliseconds == newCycleDurationInMilliseconds)
        return;
    m_cycleDurationInMilliseconds = newCycleDurationInMilliseconds;
    emit cycleDurationInMillisecondsChanged();
}

void CycleMaker::sendCommandToDevice(std::string device_name, std::string command)
{
    try {
        Tango::DeviceProxy device(device_name);
        device.command_inout(command);
    }  catch (Tango::DevFailed &e) {
        QMessageBox::information(NULL, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
    }
}

void CycleMaker::sendCommandToDevice(std::string device_name, std::string command, QVector<double> input_data)
{
    try {
        Tango::DeviceProxy device(device_name);
        device.set_timeout_millis(10000);
        Tango::DeviceData din;
        std::vector<double> temp = input_data.toStdVector();
        din << temp;
        device.command_inout_asynch(command, din, false);
    }  catch (Tango::DevFailed &e) {
        QMessageBox::information(NULL, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
    } catch (...)
    {
        QMessageBox::information(NULL, "Exception", "FAIL");
    }
}

void CycleMaker::sendCommandToDevice(std::string device_name, std::string command, float input_data)
{
    try {
        Tango::DeviceProxy device(device_name);
        Tango::DeviceData din;
        din << input_data;
        device.command_inout(command, din);
    }  catch (Tango::DevFailed &e) {
        QMessageBox::information(NULL, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
    }
}

void CycleMaker::writeAttributeData(std::string attributeName, double inputData)
{
    try
    {
        Tango::DeviceProxy device ("booster/rf/station");
        Tango::DeviceAttribute request = device.read_attribute(attributeName);
        request << inputData;
        device.write_attribute(request);
    } catch (Tango::DevFailed &e) {
        QMessageBox::information(NULL, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
    }
}

QVector<double> CycleMaker::getAttributeData(std::string attribute_name)
{
    QVector<double> readedAttribute;
    try {
        std::vector<double> readedAttributeTemp;
        Tango::DeviceProxy device ("booster/rf/station");
        Tango::DeviceAttribute reply = device.read_attribute(attribute_name);
        reply >> readedAttributeTemp;
        readedAttribute = QVector<double>::fromStdVector(readedAttributeTemp);
    } catch (Tango::DevFailed &e) {
        QMessageBox::information(NULL, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
    }
    return readedAttribute;
}

void CycleMaker::setMode(std::string mode) //derivative 0, direct 1, table 2
{
    try {
        Tango::DeviceProxy device("booster/rf/station");
        Tango::DeviceData argin;
        std::string newCmdArg = mode;
        argin << newCmdArg;
        device.command_inout("SetMode", argin);
        QString str = QString::fromStdString(mode);
        qDebug() << "setMode" << str;
    }  catch (Tango::DevFailed &e) {
        QMessageBox::information(NULL, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
    }
}

void CycleMaker::setTesterMode(bool state)
{
    try
    {
        Tango::Database db;
        Tango::DbDatum testerState("UseTester");
        Tango::DbData db_data;
        testerState << state;
        db_data.push_back(testerState);
        db.put_device_property("booster/rf/station", db_data);
    } catch (Tango::DevFailed &e) {
        QMessageBox::information(NULL, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
    }
}

void CycleMaker::sleep(ushort milliseconds_of_sleep)
{
    QThread::msleep(milliseconds_of_sleep);
}

short CycleMaker::readAttribute(std::string device_name, std::string attribute_name)
{
    short value;
    try {
        Tango::DeviceProxy device(device_name);
        Tango::DeviceAttribute reply = device.read_attribute(attribute_name);
        reply >> value;
    }   catch (Tango::DevFailed &e) {
        QMessageBox::information(NULL, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
    }
    return value;
}
