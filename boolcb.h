#ifndef BOOLCB_H
#define BOOLCB_H

#include <QObject>
#include <tango.h>

class BoolCb : public QObject, public Tango::CallBack
{
    Q_OBJECT
public:
    explicit BoolCb(QObject *parent = nullptr);
    virtual void push_event(Tango::EventData *ed);

signals:
    void boolUpdated(bool value, QString name);
};

#endif // BOOLCB_H
