#include "callback.h"
#include "mainwindow.h"

Callback::Callback(QObject *parent)
    : QObject{parent}
{

}

void Callback::push_event(Tango::EventData *ed)
{
    try
    {
        if(ed->err)
        {
            return;
        }
        else if(ed->attr_value->data_type == Tango::DEV_BOOLEAN)
        {
            bool value;
            QString name = QString::fromStdString(ed->attr_name);
            Tango::DeviceAttribute &data = *(ed->attr_value);
            data >> value;
            emit boolUpdated(value, name);
        }
        else if(ed->attr_value->data_type == Tango::DEV_DOUBLE)
        {
            double value;
            QString name = QString::fromStdString(ed->attr_name);
            Tango::DeviceAttribute &data = *(ed->attr_value);
            data >> value;
            emit dblUpdated(value, name);
        }
        else if(ed->attr_value->data_type == Tango::DEV_SHORT)
        {
            short value;
            QString name = QString::fromStdString(ed->attr_name);
            Tango::DeviceAttribute &data = *(ed->attr_value);
            data >> value;
            emit shortUpdated(value, name);
        }
    } catch(Tango::DevFailed &e)
    {
        QMessageBox::information(NULL, "Exception", QString::fromStdString(std::string(e.errors[0].desc)));
    }
}
