#include "boolcb.h"
#include <QDebug>

BoolCb::BoolCb(QObject *parent)
    : QObject{parent}
{

}

void BoolCb::push_event(Tango::EventData *ed)
{
    qDebug() << ed->attr_value->data_type;
    //if(ed->attr_value->data_type == 1) // судя по отладке под переменной типа bool подразумевается 1
    //{
    bool value;
    QString name = QString::fromStdString(ed->attr_name);
    Tango::DeviceAttribute &data = *(ed->attr_value);
    data >> value;
    emit boolUpdated(value, name);
    //}

    /*if(ed->attr_value->data_type == 5) судя по отладке под переменной типа bool подразумевается 5
    {
    double value;
    QString name = QString::fromStdString(ed->attr_name);
    Tango::DeviceAttribute &data = *(ed->attr_value);
    data >> value;
    emit dblUpdated(value, name);
    }*/
}

