QT       += core gui charts printsupport concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

win32 {
    INCLUDEPATH += $$(TANGO_ROOT_QT)/win64/include/vc16
    DEPENDPATH += $$(TANGO_ROOT_QT)/win64/include/vc16

    INCLUDEPATH += $$(TANGO_ROOT_QT)/win64/include/vc16/omniORB4
    DEPENDPATH += $$(TANGO_ROOT_QT)/win64/include/vc16/omniORB4

    INCLUDEPATH += $$(TANGO_ROOT_QT)/win64/include/vc16/COS
    DEPENDPATH += $$(TANGO_ROOT_QT)/win64/include/vc16/COS

    INCLUDEPATH += $$(TANGO_ROOT_QT)/win64/include/vc16/omnithread
    DEPENDPATH += $$(TANGO_ROOT_QT)/win64/include/vc16/omnithread

    win32:CONFIG(release, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -ltango
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -ltangod

    win32:CONFIG(release, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lzmq
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lzmqd

    win32:CONFIG(release, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lomniORB4_rt
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lomniORB4_rtd

    win32:CONFIG(release, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lomniDynamic4_rt
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lomniDynamic4_rtd

    win32:CONFIG(release, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lomnithread_rt
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lomnithread_rtd

    win32:CONFIG(release, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lCOS4_rt
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -lCOS4_rtd

    win32:CONFIG(release, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -llog4tango
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$(TANGO_ROOT_QT)/win64/lib/vc16_dll/ -llog4tangod

 }
 unix {
    INCLUDEPATH += $$(TANGO_HOME)/include/tango
    DEPENDPATH += $$(TANGO_HOME)/include/tango

    INCLUDEPATH += $$(TANGO_HOME)/include/omnithread
    DEPENDPATH += $$(TANGO_HOME)/include/omnithread

    INCLUDEPATH += $$(TANGO_HOME)/include/omniORB4
    DEPENDPATH += $$(TANGO_HOME)/include/omniORB4

    INCLUDEPATH += $$(TANGO_HOME)/include/COS
    DEPENDPATH += $$(TANGO_HOME)/include/COS

    INCLUDEPATH += -I$$(TANGO_HOME)/include
    INCLUDEPATH += -I$$(TANGO_HOME)/include

    LIBS += -L$$(TANGO_HOME)/lib/ -ltango
    LIBS += -L$$(TANGO_HOME)/lib/ -lzmq
    LIBS += -L$$(TANGO_HOME)/lib/ -lomniORB4
    LIBS += -L$$(TANGO_HOME)/lib/ -lomnithread
    LIBS += -L$$(TANGO_HOME)/lib/ -lomniDynamic4
    LIBS += -L$$(TANGO_HOME)/lib/ -lCOS4
    LIBS += -L$$(TANGO_HOME)/lib/ -llog4tango
 }

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    callback.cpp \
    cyclemaker.cpp \
    main.cpp \
    mainwindow.cpp \
    qcustomplot.cpp \
    tablemanager.cpp

HEADERS += \
    callback.h \
    cyclemaker.h \
    mainwindow.h \
    qcustomplot.h \
    tablemanager.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
