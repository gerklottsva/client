#ifndef CYCLEMAKER_H
#define CYCLEMAKER_H

#include <QObject>
#include <tango.h>
#include <QThread>
#include <QDebug>

class CycleMaker : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool cycleModeState READ cycleModeState WRITE setCycleModeState NOTIFY cycleModeStateChanged)
    Q_PROPERTY(int cycleDurationInMilliseconds READ cycleDurationInMilliseconds WRITE setCycleDurationInMilliseconds NOTIFY cycleDurationInMillisecondsChanged)
    bool m_cycleModeState;

    int m_cycleDurationInMilliseconds = 3000;

public:
    explicit CycleMaker(QObject *parent = nullptr);
    ~CycleMaker();

    bool cycleModeState() const;
    void setCycleModeState(bool newCycleModeState);

    int cycleDurationInMilliseconds() const;
    void setCycleDurationInMilliseconds(int newCycleDurationInMilliseconds);

    double deltaT_controller = 40e-6; //Отсчёт контроллера: 40 мкс

public slots:
    void makeCycle();
    void sendCommandToDevice(std::string device_name, std::string command);
    void sendCommandToDevice(std::string device_name, std::string command, QVector<double> input_data);
    void sendCommandToDevice(std::string device_name, std::string command, float input_data);
    void writeAttributeData(std::string attributeName, double inputData);
    QVector<double> getAttributeData(std::string attribute_name);
    void setMode(std::string mode);
    void setTesterMode(bool state);
    short readAttribute(std::string device_name, std::string attribute_name);
    void sleep(ushort milliseconds_of_sleep);

signals:
    void finished();

    void cycleModeStateChanged();
    void cycleDurationInMillisecondsChanged();
};

#endif // CYCLEMAKER_H
